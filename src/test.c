#include "../include/test.h"

int main(){
  //test of strcpy
  char a[] = "failed!";
  char b[] = "checked";
  printf("strcpy %s\n",strcpy(a,b));
  
  //test of strcat
  char c[] = "not ";
  char d[] = "bad!";
  printf("strcat is %s\n",strcat(c,d));

  //test of strcmp
  char e1[] = "to be";
  char e2[] = "or not";
  char e3[] = "to be";
  int res1 = strcmp(e1,e2);
  int res2 = strcmp(e2,e3);
  int res3 = strcmp(e1,e3);
  if ((res1 != 0) && (res2 !=0) && (res3 == 0))
    printf("strcmp feels good\n");
  else
    printf("strcmp is bad\n");

  //test of strlen
  char f1[] = "row row row your boat";
  char f2[] = "gently down the stream";
  char f3[] = "merrily merrily merrily merrily";
  char f4[] = "life is but a dream";
  if ((strlen(f1) == 21) && (strlen(f2) == 22) && (strlen(f3) == 31) && (strlen(f4) == 19))
    printf("strlen is OK\n");
  else
    printf("strlen failed\n");
}
