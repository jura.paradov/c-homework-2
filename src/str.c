#include "../include/str.h"
char * strcpy(char * destination, const char * source){
  char * p_d = destination;
  while (*source != 0){
    *p_d = *source;
    source++;
    p_d++;
  }
  *p_d = *source;
  return destination;
}

char * strcat(char * destination, const char * source){
  char * result = destination;
  while (*result != 0)
    result++;
  while (*source != 0){
    *result = *source;
    source++;
    result++;
  }

  *result = *source;
  return destination;
}

int strcmp(const char * str1, const char * str2){
  const char * p_1 = str1;
  const char * p_2 = str2;
  while (*p_1 == *p_2){
    p_1++;
    p_2++;
    if ((*p_1 == 0) && (*p_2 == 0))
      return 0;
  }
  return *p_1 - *p_2;
}

size_t strlen(const char * str){
  const char * it = str;
  while (*it != 0)
    ++it;
  return it - str;
}

