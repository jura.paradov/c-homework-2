all: lab2

lab2: bin/test.o bin/str.o
	gcc bin/test.o bin/str.o -o lab2

bin/test.o: src/test.c bin/str.o
	mkdir bin -p
	gcc -c src/test.c -o bin/test.o

bin/str.o: src/str.c
	mkdir bin -p
	gcc -c src/str.c -o bin/str.o

clean:
	rm -rf bin ./lab2
